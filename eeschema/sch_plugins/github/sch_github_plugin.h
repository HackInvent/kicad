/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2013 SoftPLC Corporation, Dick Hollenbeck <dick@softplc.com>
 * Copyright (C) 2016-2017 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#ifndef SCH_GITHUB_PLUGIN_H_
#define SCH_GITHUB_PLUGIN_H_


#include <memory>
#include <sch_io_mgr.h>
#include <stack>
#include <general.h>
#include <sch_plugins/legacy/sch_legacy_plugin.h>

class SCH_GITHUB_PLUGIN_CACHE;

class SCH_GITHUB_PLUGIN : public SCH_PLUGIN
{
public:


    SCH_GITHUB_PLUGIN();        // constructor, if any, must be zero arg
    virtual ~SCH_GITHUB_PLUGIN();

    const wxString GetName() const override
    {
        return wxT( "Eeschema-Legacy" );
    }

    const wxString GetFileExtension() const override
    {
        return wxT( "sch" );
    }

    const wxString GetLibraryFileExtension() const override
    {
        return wxT( "lib" );
    }

    bool IsSymbolLibWritable( const wxString& aLibraryPath ) override;

    bool IsFileChanged() const
    {
        return true;
    }

    int GetModifyHash() const override;

    void cacheLib( const wxString& aLibraryFileName );

    void EnumerateSymbolLib( wxArrayString&    aAliasNameList,
                             const wxString&   aLibraryPath,
                             const PROPERTIES* aProperties = nullptr ) override;
    void EnumerateSymbolLib( std::vector<LIB_PART*>& aSymbolList,
                                 const wxString&   aLibraryPath,
                                 const PROPERTIES* aProperties = nullptr ) override;

    LIB_PART* LoadSymbol( const wxString& aLibraryPath, const wxString& aAliasName,
                               const PROPERTIES* aProperties = nullptr ) override;

    void SaveSymbol( const wxString& aLibraryPath, const LIB_PART* aSymbol,
                     const PROPERTIES* aProperties = nullptr ) override;

protected:
    SCH_GITHUB_PLUGIN_CACHE* m_cache;
    const PROPERTIES*    m_props;      ///< Passed via Save() or Load(), no ownership, may be nullptr.


};

#endif // SCH_GITHUB_PLUGIN_H_
