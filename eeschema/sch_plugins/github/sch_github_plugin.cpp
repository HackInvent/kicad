/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2015 SoftPLC Corporation, Dick Hollenbeck <dick@softplc.com>
 * Copyright (C) 2016-2017 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#include <kicad_curl/kicad_curl_easy.h>     // Include before any wx file
#include <sstream>
#include <boost/ptr_container/ptr_map.hpp>
#include <set>

#include <wx/zipstrm.h>
#include <wx/mstream.h>
#include <wx/uri.h>


#include <sch_io_mgr.h>
#include <richio.h>
#include <sch_plugins/github/sch_github_plugin.h>
#include <macros.h>

#include <ctype.h>
#include <algorithm>
#include <boost/algorithm/string/join.hpp>

#include <wx/mstream.h>
#include <wx/filename.h>
#include <wx/tokenzr.h>
#include <pgm_base.h>
#include <gr_text.h>
#include <kiway.h>
#include <kicad_string.h>
#include <richio.h>
#include <core/typeinfo.h>
#include <properties.h>
#include <trace_helpers.h>

#include <general.h>
#include <sch_bitmap.h>
#include <sch_bus_entry.h>
#include <sch_component.h>
#include <sch_junction.h>
#include <sch_line.h>
#include <sch_marker.h>
#include <sch_no_connect.h>
#include <sch_text.h>
#include <sch_sheet.h>
#include <sch_bitmap.h>
#include <bus_alias.h>
#include <sch_plugins/legacy/sch_legacy_plugin.h>
#include <template_fieldnames.h>
#include <sch_screen.h>
#include <class_libentry.h>
#include <class_library.h>
#include <lib_arc.h>
#include <lib_bezier.h>
#include <lib_circle.h>
#include <lib_field.h>
#include <lib_pin.h>
#include <lib_polyline.h>
#include <lib_rectangle.h>
#include <lib_text.h>
#include <eeschema_id.h>       // for MAX_UNIT_COUNT_PER_PACKAGE definition
#include <symbol_lib_table.h>  // for PropPowerSymsOnly definintion.
#include <confirm.h>
#include <tool/selection.h>

#include <locale_io.h>


using namespace std;

#define Mils2Iu( x ) Mils2iu( x )
// Must be the first line of part library document (.dcm) files.
#define DOCFILE_IDENT     "EESchema-DOCLIB  Version 2.0"

#define SCH_PARSE_ERROR( text, reader, pos )                         \
    THROW_PARSE_ERROR( text, reader.GetSource(), reader.Line(),      \
                       reader.LineNumber(), pos - reader.Line() )

/**
 * Compare \a aString to the string starting at \a aLine and advances the character point to
 * the end of \a String and returns the new pointer position in \a aOutput if it is not NULL.
 *
 * @param aString - A pointer to the string to compare.
 * @param aLine - A pointer to string to begin the comparison.
 * @param aOutput - A pointer to a string pointer to the end of the comparison if not NULL.
 * @return true if \a aString was found starting at \a aLine.  Otherwise false.
 */
static bool strCompare( const char* aString, const char* aLine, const char** aOutput = NULL )
{
    size_t len = strlen( aString );
    bool retv = ( strncasecmp( aLine, aString, len ) == 0 ) &&
                ( isspace( aLine[ len ] ) || aLine[ len ] == 0 );

    if( retv && aOutput )
    {
        const char* tmp = aLine;

        // Move past the end of the token.
        tmp += len;

        // Move to the beginning of the next token.
        while( *tmp && isspace( *tmp ) )
            tmp++;

        *aOutput = tmp;
    }

    return retv;
}

/**
 * Parse an ASCII integer string with possible leading whitespace into
 * an integer and updates the pointer at \a aOutput if it is not NULL, just
 * like "man strtol()".
 *
 * @param aReader - The line reader used to generate exception throw information.
 * @param aLine - A pointer the current position in a string.
 * @param aOutput - The pointer to a string pointer to copy the string pointer position when
 *                  the parsing is complete.
 * @return A valid integer value.
 * @throw An #IO_ERROR on an unexpected end of line.
 * @throw A #PARSE_ERROR if the parsed token is not a valid integer.
 */
static int parseInt( LINE_READER& aReader, const char* aLine, const char** aOutput = NULL )
{
    if( !*aLine )
        SCH_PARSE_ERROR( _( "unexpected end of line" ), aReader, aLine );

    // Clear errno before calling strtol() in case some other crt call set it.
    errno = 0;

    long retv = strtol( aLine, (char**) aOutput, 10 );

    // Make sure no error occurred when calling strtol().
    if( errno == ERANGE )
        SCH_PARSE_ERROR( "invalid integer value", aReader, aLine );

    // strtol does not strip off whitespace before the next token.
    if( aOutput )
    {
        const char* next = *aOutput;

        while( *next && isspace( *next ) )
            next++;

        *aOutput = next;
    }

    return (int) retv;
}

class SCH_GITHUB_PLUGIN_CACHE:SCH_LEGACY_PLUGIN_CACHE {
public:
    int      m_modHash;      // Keep track of the modification status of the library.
    wxString        m_fileName;     // Absolute path and file name.
    wxFileName      m_libFileName;  // Absolute path and file name is required here.
    wxDateTime      m_fileModTime;
    int             m_versionMajor;
    int             m_versionMinor;
    bool            m_isWritable;
    bool            m_isModified;
    wxString        m_url;
    LIB_PART_MAP    m_symbols;      // Map of names of #LIB_PART pointers.
    SCH_LIB_TYPE    m_libType; // Is this cache a component or symbol library.


    SCH_GITHUB_PLUGIN_CACHE(const wxString&   aLibraryPath);
    ~SCH_GITHUB_PLUGIN_CACHE();
    void LoadRemote(const wxString& aUrl);
    void                  loadHeader( STRING_LINE_READER& aReader );
    wxDateTime GetLibModificationTime();
    // If m_libFileName is a symlink follow it to the real source file
    wxFileName GetRealFile() const;

    static void           loadAliases( std::unique_ptr<LIB_PART>& aPart, LINE_READER& aReader,
                                               LIB_PART_MAP* aMap = nullptr );
    void                  loadDocs();
};


SCH_GITHUB_PLUGIN::SCH_GITHUB_PLUGIN()
{
    m_cache = NULL;
}


SCH_GITHUB_PLUGIN::~SCH_GITHUB_PLUGIN()
{
    if(!m_cache)
        delete m_cache;
}

int SCH_GITHUB_PLUGIN::GetModifyHash() const
{

    // If the cache hasn't been loaded, it hasn't been modified.
    return 0;
}

bool SCH_GITHUB_PLUGIN::IsSymbolLibWritable( const wxString& aLibraryPath )
{
    // Writing legacy symbol libraries is deprecated.
    return false;
}

void SCH_GITHUB_PLUGIN::cacheLib( const wxString& aLibraryPath )
{
    if(!m_cache){
        delete m_cache;
        m_cache = new SCH_GITHUB_PLUGIN_CACHE( aLibraryPath );
        // Because m_cache is rebuilt, increment PART_LIBS::s_modify_generation
        // to modify the hash value that indicate component to symbol links
        // must be updated.
        PART_LIBS::s_modify_generation++;

        //
        m_cache->LoadRemote(aLibraryPath);
    }
}

void SCH_GITHUB_PLUGIN::EnumerateSymbolLib( wxArrayString&    aSymbolNameList,
                                            const wxString&   aLibraryPath,
                                            const PROPERTIES* aProperties )
{
    LOCALE_IO   toggle;     // toggles on, then off, the C locale.

    m_props = aProperties;

    bool powerSymbolsOnly = ( aProperties &&
                              aProperties->find( SYMBOL_LIB_TABLE::PropPowerSymsOnly ) != aProperties->end() );
    cacheLib( aLibraryPath );

    const LIB_PART_MAP& symbols = m_cache->m_symbols;

    for( LIB_PART_MAP::const_iterator it = symbols.begin();  it != symbols.end();  ++it )
    {
        if( !powerSymbolsOnly || it->second->IsPower() )
            aSymbolNameList.Add( it->first );
    }
}

void SCH_GITHUB_PLUGIN::EnumerateSymbolLib( std::vector<LIB_PART*>& aSymbolList,
                                            const wxString&   aLibraryPath,
                                            const PROPERTIES* aProperties )
{
    LOCALE_IO   toggle;     // toggles on, then off, the C locale.

    m_props = aProperties;

    bool powerSymbolsOnly = ( aProperties &&
                              aProperties->find( SYMBOL_LIB_TABLE::PropPowerSymsOnly ) != aProperties->end() );
    cacheLib( aLibraryPath );

    const LIB_PART_MAP& symbols = m_cache->m_symbols;

    for( LIB_PART_MAP::const_iterator it = symbols.begin();  it != symbols.end();  ++it )
    {
        if( !powerSymbolsOnly || it->second->IsPower() )
            aSymbolList.push_back( it->second );
    }
}


LIB_PART* SCH_GITHUB_PLUGIN::LoadSymbol( const wxString& aLibraryPath, const wxString& aSymbolName,
                                         const PROPERTIES* aProperties )
{
    LOCALE_IO toggle;     // toggles on, then off, the C locale.

    m_props = aProperties;

    cacheLib( aLibraryPath );

    LIB_PART_MAP::const_iterator it = m_cache->m_symbols.find( aSymbolName );

    if( it == m_cache->m_symbols.end() )
        return nullptr;

    return it->second;
}

void SCH_GITHUB_PLUGIN::SaveSymbol( const wxString& aLibraryPath, const LIB_PART* aSymbol,
                                    const PROPERTIES* aProperties )
{
    return;
}

SCH_GITHUB_PLUGIN_CACHE::SCH_GITHUB_PLUGIN_CACHE( const wxString& aFullPathAndFileName ) :
    SCH_LEGACY_PLUGIN_CACHE(aFullPathAndFileName),
    m_fileName( aFullPathAndFileName ),
    m_libFileName( aFullPathAndFileName ),
    m_isWritable( false ),
    m_isModified( false ),
    m_url(aFullPathAndFileName)
{
    m_versionMajor = -1;
    m_versionMinor = -1;
    m_libType = SCH_LIB_TYPE::LT_EESCHEMA;
    m_modHash = 1;
}

SCH_GITHUB_PLUGIN_CACHE::~SCH_GITHUB_PLUGIN_CACHE()
{
    std::vector< LIB_PART* > rootParts;

    // When the cache is destroyed, all of the alias objects on the heap should be deleted.
    for( LIB_PART_MAP::iterator it = m_symbols.begin();  it != m_symbols.end();  ++it )
        delete it->second;

    m_symbols.clear();
}

void SCH_GITHUB_PLUGIN_CACHE::loadDocs()
{
    const char* line;
    wxString    text;
    wxString    aliasName;
    wxFileName  fn = m_libFileName;
    LIB_PART*   symbol = NULL;;

    fn.SetExt( DOC_EXT );

    // Not all libraries will have a document file.
    if( !fn.FileExists() )
        return;

    if( !fn.IsFileReadable() )
        THROW_IO_ERROR( wxString::Format( _( "user does not have permission to read library "
                                             "document file \"%s\"" ), fn.GetFullPath() ) );

    FILE_LINE_READER reader( fn.GetFullPath() );

    line = reader.ReadLine();

    if( !line )
        THROW_IO_ERROR( _( "symbol document library file is empty" ) );

    if( !strCompare( DOCFILE_IDENT, line, &line ) )
        SCH_PARSE_ERROR( "invalid document library file version formatting in header",
                         reader, line );

    while( reader.ReadLine() )
    {
        line = reader.Line();

        if( *line == '#' )    // Comment line.
            continue;

        if( !strCompare( "$CMP", line, &line ) != 0 )
            SCH_PARSE_ERROR( "$CMP command expected", reader, line );

        aliasName = wxString::FromUTF8( line );
        aliasName.Trim();
        aliasName = LIB_ID::FixIllegalChars( aliasName, LIB_ID::ID_SCH );

        LIB_PART_MAP::iterator it = m_symbols.find( aliasName );

        if( it == m_symbols.end() )
            wxLogWarning( "Symbol '%s' not found in library:\n\n"
                          "'%s'\n\nat line %d offset %d", aliasName, fn.GetFullPath(),
                          reader.LineNumber(), (int) (line - reader.Line() ) );
        else
            symbol = it->second;

        // Read the curent alias associated doc.
        // if the alias does not exist, just skip the description
        // (Can happen if a .dcm is not synchronized with the corresponding .lib file)
        while( reader.ReadLine() )
        {
            line = reader.Line();

            if( !line )
                SCH_PARSE_ERROR( "unexpected end of file", reader, line );

            if( strCompare( "$ENDCMP", line, &line ) )
                break;

            text = FROM_UTF8( line + 2 );
            // Remove spaces at eol, and eol chars:
            text = text.Trim();

            switch( line[0] )
            {
            case 'D':
                if( symbol )
                    symbol->SetDescription( text );
                break;

            case 'K':
                if( symbol )
                    symbol->SetKeyWords( text );
                break;

            case 'F':
                if( symbol )
                    symbol->GetField( DATASHEET_FIELD )->SetText( text );
                break;

            case 0:
            case '\n':
            case '\r':
            case '#':
                // Empty line or commment
                break;

            default:
                SCH_PARSE_ERROR( "expected token in symbol definition", reader, line );
            }
        }
    }
}

// If m_libFileName is a symlink follow it to the real source file
wxFileName SCH_GITHUB_PLUGIN_CACHE::GetRealFile() const
{
//    wxFileName fn( m_libFileName );

//#ifndef __WINDOWS__
//    if( fn.Exists( wxFILE_EXISTS_SYMLINK ) )
//    {
//        char buffer[ PATH_MAX + 1 ];
//        ssize_t pathLen = readlink( TO_UTF8( fn.GetFullPath() ), buffer, PATH_MAX );

//        if( pathLen > 0 )
//        {
//            buffer[ pathLen ] = '\0';
//            fn.Assign( fn.GetPath() + wxT( "/" ) + wxString::FromUTF8( buffer ) );
//            fn.Normalize();
//        }
//    }
//#endif

//    return fn;

    return wxFileName();
}


wxDateTime SCH_GITHUB_PLUGIN_CACHE::GetLibModificationTime()
{
//    wxFileName fn = GetRealFile();

//    // update the writable flag while we have a wxFileName, in a network this
//    // is possibly quite dynamic anyway.
//    m_isWritable = fn.IsFileWritable();

//    return fn.GetModificationTime();
    return wxDateTime();
}

void SCH_GITHUB_PLUGIN_CACHE::loadHeader( STRING_LINE_READER& aReader )
{
    const char* line = aReader.Line();

    wxASSERT( strCompare( "$HEADER", line, &line ) );

    while( aReader.ReadLine() )
    {
        line = (char*) aReader;

        // The time stamp saved in old library files is not used or saved in the latest
        // library file version.
        if( strCompare( "TimeStamp", line, &line ) )
            continue;
        else if( strCompare( "$ENDHEADER", line, &line ) )
            return;
    }

    SCH_PARSE_ERROR( "$ENDHEADER not found", aReader, line );
}


void SCH_GITHUB_PLUGIN_CACHE::LoadRemote(const wxString& aURL){

    KICAD_CURL_EASY kcurl;      // this can THROW_IO_ERROR

    kcurl.SetURL( aURL.ToStdString() );
    kcurl.SetUserAgent( "http://kicad-pcb.org" );
    kcurl.SetHeader( "Accept", "application/zip" );
    kcurl.SetFollowRedirects( true );

    std::string filecontent;

    try
    {
        kcurl.Perform();
        filecontent = kcurl.GetBuffer();

        // check the status
        if( ( filecontent.compare( 0, 9, "Not Found", 9 ) == 0 ) ||
            ( filecontent.compare( 0, 14, "404: Not Found", 14 ) == 0 ) )
        {
            UTF8 fmt( _( "Cannot download file '%s'.\nThe file does not exist on the server" ) );
            std::string msg = StrPrintf( fmt.c_str(),(const char*) aURL.utf8_str() );

            THROW_IO_ERROR( msg );
        }

    }
    catch( const IO_ERROR& ioe )
    {
        // https "GET" has failed, report this to API caller.
        // Note: kcurl.Perform() does not return an error if the file to download is not found
        static const char errorcmd[] = "http GET command failed";  // Do not translate this message

        UTF8 fmt( _( "%s\nCannot get/download remote file : '%s'\n.\nReason: '%s'" ) );

        std::string msg = StrPrintf( fmt.c_str(),
                                     errorcmd,
                                     aURL.ToStdString().c_str(),
                                     (const char*) ioe.What().utf8_str()
                                     );

        THROW_IO_ERROR( msg );
    }

    STRING_LINE_READER reader(filecontent,aURL);

    if( !reader.ReadLine() )
        THROW_IO_ERROR( _( "unexpected end of file" ) );

    const char* line = reader.Line();

    if( !strCompare( "EESchema-LIBRARY Version", line, &line ) )
    {
        // Old .sym files (which are libraries with only one symbol, used to store and reuse shapes)
        // EESchema-LIB Version x.x SYMBOL. They are valid files.
        if( !strCompare( "EESchema-LIB Version", line, &line ) )
            SCH_PARSE_ERROR( "file is not a valid component or symbol library file", reader, line );
    }

    m_versionMajor = parseInt( reader, line, &line );

    if( *line != '.' )
        SCH_PARSE_ERROR( "invalid file version formatting in header", reader, line );

    m_versionMinor = parseInt( reader, line, &line );

    if( m_versionMajor < 1 || m_versionMinor < 0 || m_versionMinor > 99 )
        SCH_PARSE_ERROR( "invalid file version in header", reader, line );

    // Check if this is a symbol library which is the same as a component library but without
        // any alias, documentation, footprint filters, etc.
        if( strCompare( "SYMBOL", line, &line ) )
        {
            // Symbol files add date and time stamp info to the header.
            m_libType = SCH_LIB_TYPE::LT_SYMBOL;

            /// @todo Probably should check for a valid date and time stamp even though it's not used.
        }
        else
        {
            m_libType = SCH_LIB_TYPE::LT_EESCHEMA;
        }

        while( reader.ReadLine() )
        {
            line = reader.Line();

            if( *line == '#' || isspace( *line ) )  // Skip comments and blank lines.
                continue;

            // Headers where only supported in older library file formats.
            if( m_libType == SCH_LIB_TYPE::LT_EESCHEMA && strCompare( "$HEADER", line ) )
                loadHeader( reader );

            if( strCompare( "DEF", line ) )
            {
                // Read one DEF/ENDDEF part entry from library:
                LIB_PART* part = LoadPart( reader, m_versionMajor, m_versionMinor, &m_symbols );

                m_symbols[ part->GetName() ] = part;
            }
        }

        ++m_modHash;

        // Remember the file modification time of library file when the
        // cache snapshot was made, so that in a networked environment we will
        // reload the cache as needed.
        m_fileModTime = GetLibModificationTime();

        if( USE_OLD_DOC_FILE_FORMAT( m_versionMajor, m_versionMinor ) )
            loadDocs();

}





