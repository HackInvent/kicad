#ifndef _SCH_LEGACY_PLUGIN_H_
#define _SCH_LEGACY_PLUGIN_H_

/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2016 CERN
 * Copyright (C) 2016-2019 KiCad Developers, see change_log.txt for contributors.
 *
 * @author Wayne Stambaugh <stambaughw@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <memory>
#include <sch_io_mgr.h>
#include <stack>
#include <general.h>        // for EESCHEMA_VERSION definition

#include <wx/mstream.h>
#include <wx/filename.h>
#include <wx/tokenzr.h>
#include <pgm_base.h>
#include <gr_text.h>
#include <kiway.h>
#include <kicad_string.h>
#include <richio.h>
#include <core/typeinfo.h>
#include <properties.h>
#include <trace_helpers.h>

#include <general.h>
#include <sch_bitmap.h>
#include <sch_bus_entry.h>
#include <sch_component.h>
#include <sch_junction.h>
#include <sch_line.h>
#include <sch_marker.h>
#include <sch_no_connect.h>
#include <sch_text.h>
#include <sch_sheet.h>
#include <sch_bitmap.h>
#include <bus_alias.h>
#include <sch_plugins/legacy/sch_legacy_plugin.h>
#include <template_fieldnames.h>
#include <sch_screen.h>
#include <class_libentry.h>
#include <class_library.h>
#include <lib_arc.h>
#include <lib_bezier.h>
#include <lib_circle.h>
#include <lib_field.h>
#include <lib_pin.h>
#include <lib_polyline.h>
#include <lib_rectangle.h>
#include <lib_text.h>
#include <eeschema_id.h>       // for MAX_UNIT_COUNT_PER_PACKAGE definition
#include <symbol_lib_table.h>  // for PropPowerSymsOnly definintion.
#include <confirm.h>
#include <tool/selection.h>


class KIWAY;
class LINE_READER;
class SCH_SCREEN;
class SCH_SHEET;
class SCH_BITMAP;
class SCH_JUNCTION;
class SCH_NO_CONNECT;
class SCH_LINE;
class SCH_BUS_ENTRY_BASE;
class SCH_TEXT;
class SCH_COMPONENT;
class SCH_FIELD;
class PROPERTIES;
class SELECTION;
class SCH_LEGACY_PLUGIN_CACHE;
class LIB_PART;
class PART_LIB;
class BUS_ALIAS;


/**
 * A #SCH_PLUGIN derivation for loading schematic files created before the new s-expression
 * file format.
 *
 * The legacy parser and formatter attempt to be compatible with the legacy file format.
 * The original parser was very forgiving in that it would parse only part of a keyword.
 * So "$C", "$Co", and "$Com" could be used for "$Comp" and the old parser would allow
 * this.  This parser is not that forgiving and sticks to the legacy file format document.
 *
 * As with all SCH_PLUGINs there is no UI dependencies i.e. windowing calls allowed.
 */
class SCH_LEGACY_PLUGIN : public SCH_PLUGIN
{
public:

    SCH_LEGACY_PLUGIN();
    virtual ~SCH_LEGACY_PLUGIN();

    const wxString GetName() const override
    {
        return wxT( "Eeschema-Legacy" );
    }

    const wxString GetFileExtension() const override
    {
        return wxT( "sch" );
    }

    const wxString GetLibraryFileExtension() const override
    {
        return wxT( "lib" );
    }

    /**
     * The property used internally by the plugin to enable cache buffering which prevents
     * the library file from being written every time the cache is changed.  This is useful
     * when writing the schematic cache library file or saving a library to a new file name.
     */
    static const char* PropBuffering;

    /**
     * The property used internally by the plugin to disable writing the library
     * documentation (.dcm) file when saving the library cache.
     */
    static const char* PropNoDocFile;

    int GetModifyHash() const override;

    SCH_SHEET* Load( const wxString& aFileName, SCHEMATIC* aSchematic,
                     SCH_SHEET* aAppendToMe = nullptr,
                     const PROPERTIES* aProperties = nullptr ) override;

    void LoadContent( LINE_READER& aReader, SCH_SCREEN* aScreen,
                      int version = EESCHEMA_VERSION );

    void Save( const wxString& aFileName, SCH_SHEET* aScreen, SCHEMATIC* aSchematic,
               const PROPERTIES* aProperties = nullptr ) override;

    void Format( SCH_SHEET* aSheet );

    void Format( SELECTION* aSelection, OUTPUTFORMATTER* aFormatter );

    void EnumerateSymbolLib( wxArrayString&    aSymbolNameList,
                             const wxString&   aLibraryPath,
                             const PROPERTIES* aProperties = nullptr ) override;
    void EnumerateSymbolLib( std::vector<LIB_PART*>& aSymbolList,
                             const wxString&   aLibraryPath,
                             const PROPERTIES* aProperties = nullptr ) override;
    LIB_PART* LoadSymbol( const wxString& aLibraryPath, const wxString& aAliasName,
                           const PROPERTIES* aProperties = nullptr ) override;
    void SaveSymbol( const wxString& aLibraryPath, const LIB_PART* aSymbol,
                     const PROPERTIES* aProperties = nullptr ) override;
    void DeleteSymbol( const wxString& aLibraryPath, const wxString& aSymbolName,
                       const PROPERTIES* aProperties = nullptr ) override;
    void CreateSymbolLib( const wxString& aLibraryPath,
                          const PROPERTIES* aProperties = nullptr ) override;
    bool DeleteSymbolLib( const wxString& aLibraryPath,
                          const PROPERTIES* aProperties = nullptr ) override;
    void SaveLibrary( const wxString& aLibraryPath,
                      const PROPERTIES* aProperties = nullptr ) override;

    bool CheckHeader( const wxString& aFileName ) override;
    bool IsSymbolLibWritable( const wxString& aLibraryPath ) override;

    const wxString& GetError() const override { return m_error; }

    static LIB_PART* ParsePart( LINE_READER& aReader, int majorVersion = 0, int minorVersion = 0 );
    static void FormatPart( LIB_PART* aPart, OUTPUTFORMATTER& aFormatter );

private:
    void loadHierarchy( SCH_SHEET* aSheet );
    void loadHeader( LINE_READER& aReader, SCH_SCREEN* aScreen );
    void loadPageSettings( LINE_READER& aReader, SCH_SCREEN* aScreen );
    void loadFile( const wxString& aFileName, SCH_SCREEN* aScreen );
    SCH_SHEET* loadSheet( LINE_READER& aReader );
    SCH_BITMAP* loadBitmap( LINE_READER& aReader );
    SCH_JUNCTION* loadJunction( LINE_READER& aReader );
    SCH_NO_CONNECT* loadNoConnect( LINE_READER& aReader );
    SCH_LINE* loadWire( LINE_READER& aReader );
    SCH_BUS_ENTRY_BASE* loadBusEntry( LINE_READER& aReader );
    SCH_TEXT* loadText( LINE_READER& aReader );
    SCH_COMPONENT* loadComponent( LINE_READER& aReader );
    std::shared_ptr<BUS_ALIAS> loadBusAlias( LINE_READER& aReader, SCH_SCREEN* aScreen );

    void saveComponent( SCH_COMPONENT* aComponent );
    void saveField( SCH_FIELD* aField );
    void saveBitmap( SCH_BITMAP* aBitmap );
    void saveSheet( SCH_SHEET* aSheet );
    void saveJunction( SCH_JUNCTION* aJunction );
    void saveNoConnect( SCH_NO_CONNECT* aNoConnect );
    void saveBusEntry( SCH_BUS_ENTRY_BASE* aBusEntry );
    void saveLine( SCH_LINE* aLine );
    void saveText( SCH_TEXT* aText );
    void saveBusAlias( std::shared_ptr<BUS_ALIAS> aAlias );

    void cacheLib( const wxString& aLibraryFileName );
    bool writeDocFile( const PROPERTIES* aProperties );
    bool isBuffering( const PROPERTIES* aProperties );

protected:
    int                  m_version;    ///< Version of file being loaded.

    /** For throwing exceptions or errors on partial schematic loads. */
    wxString             m_error;

    wxString             m_path;       ///< Root project path for loading child sheets.
    std::stack<wxString> m_currentPath;///< Stack to maintain nested sheet paths
    const PROPERTIES*    m_props;      ///< Passed via Save() or Load(), no ownership, may be nullptr.
    SCH_SHEET*           m_rootSheet;  ///< The root sheet of the schematic being loaded..
    OUTPUTFORMATTER*     m_out;        ///< The output formatter for saving SCH_SCREEN objects.
    SCH_LEGACY_PLUGIN_CACHE* m_cache;
    SCHEMATIC*          m_schematic;   ///< Passed to Load(), the schematic object being loaded

    /// initialize PLUGIN like a constructor would.
    void init( SCHEMATIC* aSchematic, const PROPERTIES* aProperties = nullptr );
};

/**
 * A cache assistant for the part library portion of the #SCH_PLUGIN API, and only for the
 * #SCH_LEGACY_PLUGIN, so therefore is private to this implementation file, i.e. not placed
 * into a header.
 */
class SCH_LEGACY_PLUGIN_CACHE
{
    static int      m_modHash;      // Keep track of the modification status of the library.

        wxString        m_fileName;     // Absolute path and file name.
        wxFileName      m_libFileName;  // Absolute path and file name is required here.
        wxDateTime      m_fileModTime;
        LIB_PART_MAP    m_symbols;      // Map of names of #LIB_PART pointers.
        bool            m_isWritable;
        bool            m_isModified;
        int             m_versionMajor;
        int             m_versionMinor;
        SCH_LIB_TYPE    m_libType; // Is this cache a component or symbol library.

        void                  loadHeader( FILE_LINE_READER& aReader );
        static void           loadAliases( std::unique_ptr<LIB_PART>& aPart, LINE_READER& aReader,
                                           LIB_PART_MAP* aMap = nullptr );
        static void           loadField( std::unique_ptr<LIB_PART>& aPart, LINE_READER& aReader );
        static void           loadDrawEntries( std::unique_ptr<LIB_PART>& aPart, LINE_READER& aReader,
                                               int aMajorVersion, int aMinorVersion );
        static void           loadFootprintFilters( std::unique_ptr<LIB_PART>& aPart,
                                                    LINE_READER& aReader );
        void                  loadDocs();
        static LIB_ARC*       loadArc( std::unique_ptr<LIB_PART>& aPart, LINE_READER& aReader );
        static LIB_CIRCLE*    loadCircle( std::unique_ptr<LIB_PART>& aPart, LINE_READER& aReader );
        static LIB_TEXT*      loadText( std::unique_ptr<LIB_PART>& aPart, LINE_READER& aReader,
                                        int aMajorVersion, int aMinorVersion );
        static LIB_RECTANGLE* loadRectangle( std::unique_ptr<LIB_PART>& aPart,
                                             LINE_READER& aReader );
        static LIB_PIN*       loadPin( std::unique_ptr<LIB_PART>& aPart, LINE_READER& aReader );
        static LIB_POLYLINE*  loadPolyLine( std::unique_ptr<LIB_PART>& aPart, LINE_READER& aReader );
        static LIB_BEZIER*    loadBezier( std::unique_ptr<LIB_PART>& aPart, LINE_READER& aReader );

        static FILL_TYPE   parseFillMode( LINE_READER& aReader, const char* aLine,
                                       const char** aOutput );
        LIB_PART*       removeSymbol( LIB_PART* aAlias );

        void            saveDocFile();
        static void     saveArc( LIB_ARC* aArc, OUTPUTFORMATTER& aFormatter );
        static void     saveBezier( LIB_BEZIER* aBezier, OUTPUTFORMATTER& aFormatter );
        static void     saveCircle( LIB_CIRCLE* aCircle, OUTPUTFORMATTER& aFormatter );
        static void     saveField( LIB_FIELD* aField, OUTPUTFORMATTER& aFormatter );
        static void     savePin( LIB_PIN* aPin, OUTPUTFORMATTER& aFormatter );
        static void     savePolyLine( LIB_POLYLINE* aPolyLine, OUTPUTFORMATTER& aFormatter );
        static void     saveRectangle( LIB_RECTANGLE* aRectangle, OUTPUTFORMATTER& aFormatter );
        static void     saveText( LIB_TEXT* aText, OUTPUTFORMATTER& aFormatter );

        friend SCH_LEGACY_PLUGIN;

    public:
        SCH_LEGACY_PLUGIN_CACHE( const wxString& aLibraryPath );
        ~SCH_LEGACY_PLUGIN_CACHE();

        int GetModifyHash() const { return m_modHash; }

        // Most all functions in this class throw IO_ERROR exceptions.  There are no
        // error codes nor user interface calls from here, nor in any SCH_PLUGIN objects.
        // Catch these exceptions higher up please.

        /// Save the entire library to file m_libFileName;
        void Save( bool aSaveDocFile = true );

        void Load();

        void AddSymbol( const LIB_PART* aPart );

        void DeleteSymbol( const wxString& aName );

        // If m_libFileName is a symlink follow it to the real source file
        wxFileName GetRealFile() const;

        wxDateTime GetLibModificationTime();

        bool IsFile( const wxString& aFullPathAndFileName ) const;

        bool IsFileChanged() const;

        void SetModified( bool aModified = true ) { m_isModified = aModified; }

        wxString GetLogicalName() const { return m_libFileName.GetName(); }

        void SetFileName( const wxString& aFileName ) { m_libFileName = aFileName; }

        wxString GetFileName() const { return m_libFileName.GetFullPath(); }

        static LIB_PART* LoadPart( LINE_READER& aReader, int aMajorVersion, int aMinorVersion,
                                   LIB_PART_MAP* aMap = nullptr );
        static void      SaveSymbol( LIB_PART* aSymbol, OUTPUTFORMATTER& aFormatter,
                                     LIB_PART_MAP* aMap = nullptr );

};

#endif  // _SCH_LEGACY_PLUGIN_H_
